# c6i.8xlarge Amazon EC2 instance (Ice Lake architecture, 16 physical cores, 32 vCPU)
# Ubuntu 20.04

sudo apt-get update 
sudo apt-get install python3-pip -y
pip install pip --upgrade
export PATH=/home/ubuntu/.local/bin:$PATH
pip install virtualenv

virtualenv openvino
source openvino/bin/activate
pip install -r requirements.txt
